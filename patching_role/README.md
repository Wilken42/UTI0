APP Patching example Role
=========

An example role for being used in the patching process.

this role was created by the command line command "ansible-galaxy init app_role_example"


Role Variables
--------------

action - this is set to { stop, start, do }

This dictates what action is done in this role, each one calling a different task list.


Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

- name: Linux Patching App Roll playbook
  hosts: client
  #  any_errors_fatal: true
  #  max_fail_percentage: 2
  gather_facts: yes
  vars:
    examplevar: 4
  tasks:
    - name: Stop the AppID services
      include_role:
        name: { role: downtime, action: stop }
    - name: Do something during patching for the AppID
      include_role:
        name: { role: app_role_example, action: do }
    - name: Start the AppID services
      include_role:
        name: { role: app_role_example, action: start }
